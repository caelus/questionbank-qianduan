import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import axios from 'axios'
import moment from 'moment'
import "chart.js"

import App from './App.vue'
import router from './router'

const app = createApp(App)
//axios全局配置
axios.defaults.baseURL="/api"
axios.defaults.timeout=50000
// axios.defaults.headers.common['uuid']=localStorage.getItem("uuid")
axios.defaults.headers.common['token']=localStorage.getItem("token")
app.config.globalProperties.$axios=axios

app.use(createPinia())
app.use(router)
app.use(ElementPlus)
app.use(moment)


app.mount('#app')
