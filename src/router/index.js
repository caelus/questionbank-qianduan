import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      meta: { keepAlive: false, title: '主页' },
      beforeEnter: (from, to, next) => {
        console.log("欢迎使用试题库管理系统！")
        let role = localStorage.getItem("role")
        if (role == 1) {
          console.log("学生")
          next('/stu')
        } else if (role == 2) {
          console.log("教师")
          next('/tea')
        } else if (role == 3) {
          console.log("管理员！")
          next('/admin');
        } else {
          next();
        }
      },
      component: () => import('../views/Home.vue')
    },
    {
      path: '/index',
      name: 'index',
      component: () => import('../views/Home.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/Login.vue')
    },
    {
      path: '/signup',
      name: 'signup',
      component: () => import('../views/Signup.vue')
    },
    {
      path: '/help',
      name: 'help',
      component: () => import('../views/Help.vue')
    },
    {
      path: '/admin',
      name: 'admin',
      beforeEnter: (from, to, next) => {
        let role = localStorage.getItem("role")
        if (role != 3) {
          console.log("你TM不是管理员！")
          next('/login')
        } else {
          console.log("欢迎管理员大人！")
          next()
        }
      },
      component: () => import('../views/admin/Admin.vue'),
      children: [
        {//新建用户
          path: '/admin/user/add',
          name: 'adminUserAdd',
          component: () => import('../views/admin/user/UserAdd.vue')
        },
        {//用户列表
          path: '/admin/user/list',
          name: 'adminUserList',
          component: () => import('../views/admin/user/UserList.vue')
        },
        {//学生列表
          path: '/admin/user/student',
          name: 'adminUserStudent',
          component: () => import('../views/admin/user/UserStudent.vue')
        },
        {//教师列表
          path: '/admin/user/teacher',
          name: 'adminUserTeacher',
          component: () => import('../views/admin/user/UserTeacher.vue')
        },
        {//班级列表
          path: '/admin/class/list',
          name: 'adminClassList',
          component: () => import('../views/admin/class/ClassList.vue')
        },
        {//添加班级
          path: '/admin/class/add',
          name: 'adminClassAdd',
          component: () => import('../views/admin/class/ClassAdd.vue')
        },
        {//删改班级
          path: '/admin/class/update',
          name: 'adminClassUpdate',
          component: () => import('../views/admin/class/ClassUpdate.vue')
        },
        {//科目列表
          path: '/admin/subject/list',
          name: 'adminSubjectList',
          component: () => import('../views/admin/subject/SubjectList.vue')
        },
        {//添加科目
          path: '/admin/subject/add',
          name: 'adminSubjectAdd',
          component: () => import('../views/admin/subject/SubjectAdd.vue')
        },
        {//删改科目
          path: '/admin/subject/update',
          name: 'adminSubjectUpdate',
          component: () => import('../views/admin/subject/SubjectUpdate.vue')
        },
        {//写信息
          path: '/admin/message/add',
          name: 'adminMessageAdd',
          component: () => import('../views/admin/message/MessageAdd.vue')
        },
        {//收件箱
          path: '/admin/message/receive',
          name: 'adminMessageReceive',
          component: () => import('../views/admin/message/MessageReceive.vue')
        },
        {//发件箱
          path: '/admin/message/send',
          name: 'adminMessageSend',
          component: () => import('../views/admin/message/MessageSend.vue')
        },
        {//试卷列表
          path: '/admin/paper/search',
          name: 'adminPaperSearch',
          component: () => import('../views/admin/paper/PaperSearch.vue')
        },
        {//试卷详细
          path: '/admin/paper/detail',
          name: 'adminPaperDetail',
          component: () => import('../views/admin/paper/PaperDetail.vue')
        },
        {//试卷详细带有id
          path: '/admin/paper/detail/:id',
          component: () => import('../views/admin/paper/PaperDetail.vue')
        },
        {//问题列表
          path: '/admin/question/search',
          name: 'adminQuestionSearch',
          component: () => import('../views/admin/question/QuestionSearch.vue')
        },
        {//问题详细
          path: '/admin/question/detail',
          name: 'adminQuestionDetail',
          component: () => import('../views/admin/question/QuestionDetail.vue')
        },
        {//问题详细带有id
          path: '/admin/question/detail/:id',
          component: () => import('../views/admin/question/QuestionDetail.vue')
        },
        {//统计学生
          path: '/admin/static/student',
          name: 'adminStaticStudent',
          component: () => import('../views/admin/static/StaticStudent.vue')
        },
        {//统计教师
          path: '/admin/static/teacher',
          name: 'adminStaticTeacher',
          component: () => import('../views/admin/static/StaticTeacher.vue')
        },
        {//统计试卷1
          path: '/admin/static/paper1',
          name: 'adminStaticPaper1',
          component: () => import('../views/admin/static/StaticPaper1.vue')
        },
        {//统计试卷s
          path: '/admin/static/papers',
          name: 'adminStaticPapers',
          component: () => import('../views/admin/static/StaticPapers.vue')
        },
        {//统计问题
          path: '/admin/staitc/questions',
          name: 'adminStaticQuestions',
          component: () => import('../views/admin/static/StaticQuestions.vue')
        },
        {//统计成绩
          path: '/admin/staitc/report',
          name: 'adminStaticReport',
          component: () => import('../views/admin/static/StaticReport.vue')
        },
        {//管理员 我的
          path: '/admin/my/my',
          name: 'adminMyMy',
          component: () => import('../views/admin/my/My.vue')
        }
      ]
    },
    {
      path: '/stu',
      name: 'stu',
      beforeEnter: (from, to, next) => {
        let role = localStorage.getItem("role")
        if (role != 1) {
          console.log("非学生用户！")
          next('/login')
        } else {
          console.log("欢迎学生！")
          next()
        }
      },
      component: () => import('../views/stu/Stu.vue'),
      children: [
        //----------------------------------------个人中心
        {//个人中心
          path: '/stu/my',
          component: () => import('../views/stu/my/My.vue')
        },
        //----------------------------------------考试
        {//试卷列表
          path: '/stu/paper/list',
          component: () => import('../views/stu/paper/List.vue')
        },
        {//答题 根
          path: '/stu/paper/answer',
          component: () => import('../views/stu/paper/Answer.vue')
        },
        {//答题 试卷首页
          path: '/stu/paper/exam/:id',
          component: () => import('../views/stu/paper/Exam.vue'),
          children: [
            {//答题 试卷题目
              path: '/stu/paper/exam/:id/:pqid',
              component: () => import('../views/stu/paper/Question.vue'),
            },
          ]
        },
        {//统计学生
          path: '/stu/static/student',
          component: () => import('../views/stu/static/StaticStudent.vue')
        },
        {//统计教师
          path: '/stu/static/teacher',
          component: () => import('../views/stu/static/StaticTeacher.vue')
        },
        {//统计单试卷
          path: '/stu/static/paper1',
          component: () => import('../views/stu/static/StaticPaper1.vue')
        },
        {//统计所有试卷
          path: '/stu/static/papers',
          component: () => import('../views/stu/static/StaticPapers.vue')
        },
        {//统计试题
          path: '/stu/staitc/questions',
          component: () => import('../views/stu/static/StaticQuestions.vue')
        },
        {//统计成绩
          path: '/stu/staitc/report',
          component: () => import('../views/stu/static/StaticReport.vue')
        },
        {//查询班级列表
          path: '/stu/general/class',
          component: () => import('../views/stu/general/ClassList.vue')
        },
        {//查询科目列表
          path: '/stu/general/subject',
          component: () => import('../views/stu/general/SubjectList.vue')
        },
        {//写消息
          path: '/stu/message/add',
          component: () => import('../views/stu/message/MessageAdd.vue')
        },
        {//收件箱
          path: '/stu/message/receive',
          component: () => import('../views/stu/message/MessageReceive.vue')
        },
        {//发件箱
          path: '/stu/message/send',
          component: () => import('../views/stu/message/MessageSend.vue')
        },
        {//我的成绩单
          path: '/stu/report/report',
          component: () => import('../views/stu/report/Report.vue')
        },
        {//我的试卷
          path: '/stu/report/mypaper',
          component: () => import('../views/stu/report/Mypaper.vue')
        },
        {//我的试卷（通过id）
          path: '/stu/report/mypaper/:id',
          component: () => import('../views/stu/report/Mypaper.vue')
        },
      ]
    },
    {
      path: '/tea',
      name: 'tea',
      beforeEnter: (from, to, next) => {
        let role = localStorage.getItem("role")
        if (role != 2) {
          console.log("非教师用户！")
          next('/login')
        } else {
          console.log("欢迎教师！")
          next()
        }
      },
      component: () => import('../views/tea/Tea.vue'),
      children: [
        //-----------------个人中心
        {//个人信息
          path: '/tea/my',
          component: () => import('../views/tea/my/My.vue')
          //注意：是component不是components
        },
        //-------------------通用功能
        {//统计学生
          path: '/tea/static/student',
          component: () => import('../views/tea/static/StaticStudent.vue')
        },
        {//统计教师
          path: '/tea/static/teacher',
          component: () => import('../views/tea/static/StaticTeacher.vue')
        },
        {//统计单试卷
          path: '/tea/static/paper1',
          component: () => import('../views/tea/static/StaticPaper1.vue')
        },
        {//统计所有试卷
          path: '/tea/static/papers',
          component: () => import('../views/tea/static/StaticPapers.vue')
        },
        {//统计试题
          path: '/tea/staitc/questions',
          component: () => import('../views/tea/static/StaticQuestions.vue')
        },
        {//统计成绩
          path: '/tea/staitc/report',
          component: () => import('../views/tea/static/StaticReport.vue')
        },
        {//查询班级列表
          path: '/tea/general/class',
          component: () => import('../views/tea/general/ClassList.vue')
        },
        {//查询科目列表
          path: '/tea/general/subject',
          component: () => import('../views/tea/general/SubjectList.vue')
        },
        {//写消息
          path: '/tea/message/add',
          component: () => import('../views/tea/message/MessageAdd.vue')
        },
        {//收件箱
          path: '/tea/message/receive',
          component: () => import('../views/tea/message/MessageReceive.vue')
        },
        {//发件箱
          path: '/tea/message/send',
          component: () => import('../views/tea/message/MessageSend.vue')
        },
        //--------------------------------试题
        {//试题搜索
          path: '/tea/question/search',
          component: () => import('../views/tea/question/Search.vue')
        },
        {//我的试题搜索
          path: '/tea/question/mysearch',
          component: () => import('../views/tea/question/MySearch.vue')
        },
        {//新增试题
          path: '/tea/question/add',
          component: () => import('../views/tea/question/Add.vue')
        },
        {//修改试题
          path: '/tea/question/update',
          component: () => import('../views/tea/question/Update.vue')
        },
        {//删除试题
          path: '/tea/question/delete',
          component: () => import('../views/tea/question/Delete.vue')
        },
        {//试题详情（菜单不可见）
          path: '/tea/question/detail/:id',
          component: () => import('../views/tea/question/QuestionDetail.vue')
        },
        {//修改试题（查询跳转）
          path: '/tea/question/update/:id',
          component: () => import('../views/tea/question/Update.vue')
        },
        //----------------------------------------试卷
        {//搜索
          path: '/tea/paper/search',
          component: () => import('../views/tea/paper/Search.vue')
        },
        {//搜索我的
          path: '/tea/paper/mysearch',
          component: () => import('../views/tea/paper/MySearch.vue')
        },
        {//新建试卷
          path: '/tea/paper/add',
          component: () => import('../views/tea/paper/Add.vue')
        },
        {//修改试卷
          path: '/tea/paper/update',
          component: () => import('../views/tea/paper/Update.vue')
        },
        {//删除试卷
          path: '/tea/paper/delete',
          component: () => import('../views/tea/paper/Delete.vue')
        },
        {//审查试卷
          path: '/tea/paper/check',
          component: () => import('../views/tea/paper/Check.vue')
        },
        {//试卷详情
          path: '/tea/paper/detail',
          component: () => import('../views/tea/paper/Detail.vue')
        },
        {//试卷详情（通过查询）
          path: '/tea/paper/detail/:id',
          component: () => import('../views/tea/paper/Detail.vue')
        },
        {//修改试卷（通过查询）
          path: '/tea/paper/update/:id',
          component: () => import('../views/tea/paper/Update.vue')
        },
        //-----------------------------阅卷
        {//阅卷准备菜单
          path: '/tea/exam/ready',
          component: () => import('../views/tea/exam/Ready.vue')
        },
        {//阅卷试卷
          path: '/tea/exam/exam/:pid/:sid',
          component: () => import('../views/tea/exam/Exam.vue'),
          children: [
            {
              path: '/tea/exam/exam/:pid/:sid/:pqid',
              component: () => import('../views/tea/exam/Question.vue')
            }
          ]
        },
        {//刷新阅卷成绩
          path: '/tea/exam/refresh',
          component: () => import('../views/tea/exam/Refresh.vue')
        },
        //-------------------------------专用查询
        {
          //查询学生列表
          path: '/tea/query/stu',
          component: () => import('../views/tea/query/Stu.vue')
        },
        {
          //查询班级学生
          path: '/tea/query/classstu',
          component: () => import('../views/tea/query/ClassStu.vue')
        },
        {
          //查询考试成绩单
          path: '/tea/query/report',
          component: () => import('../views/tea/query/Report.vue')
        },
        {
          //查询成绩单详情
          path: '/tea/query/reportdetail',
          component: () => import('../views/tea/query/ReportDetail.vue')
        },
        {
          //查询成绩单详情（根据id）
          path: '/tea/query/reportdetail/:id',
          component: () => import('../views/tea/query/ReportDetail.vue')
        },
      ]
    }
  ]
})

// router.beforeEach((to,from,next)=>{

// })

export default router
