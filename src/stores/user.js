import { defineStore } from 'pinia'

export const useUserStore = defineStore("user", {
    state: () => ({
        uid:'',
        role:'',
        sid:'',
        tid:'',
    }),
    getters: {
        
    },
    actions: {
        
    },
})
