/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80036
 Source Host           : localhost:3306
 Source Schema         : question-bank

 Target Server Type    : MySQL
 Target Server Version : 80036
 File Encoding         : 65001

 Date: 15/05/2024 21:09:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for answer
-- ----------------------------
DROP TABLE IF EXISTS `answer`;
CREATE TABLE `answer`  (
  `ans_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '答案id',
  `ans_pap_que_id` int(0) NOT NULL COMMENT '考试试题id',
  `ans_stu_id` int(0) NOT NULL COMMENT '学生id',
  `ans_tea_id` int(0) NULL DEFAULT NULL COMMENT '教师id',
  `ans_detail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '回答详情',
  `ans_score` int(0) NULL DEFAULT NULL COMMENT '得分',
  `ans_note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
  PRIMARY KEY (`ans_id`) USING BTREE,
  UNIQUE INDEX `ans_pap_que_id_2`(`ans_pap_que_id`, `ans_stu_id`) USING BTREE,
  INDEX `ans_pap_que_id`(`ans_pap_que_id`) USING BTREE,
  INDEX `ans_stu_id`(`ans_stu_id`) USING BTREE,
  INDEX `ans_tea_id`(`ans_tea_id`) USING BTREE,
  CONSTRAINT `answer_ibfk_1` FOREIGN KEY (`ans_pap_que_id`) REFERENCES `pap_que` (`pq_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `answer_ibfk_2` FOREIGN KEY (`ans_stu_id`) REFERENCES `student` (`stu_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `answer_ibfk_3` FOREIGN KEY (`ans_tea_id`) REFERENCES `teacher` (`tea_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '答案' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for class
-- ----------------------------
DROP TABLE IF EXISTS `class`;
CREATE TABLE `class`  (
  `class_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '班级id',
  `class_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '班级名称',
  PRIMARY KEY (`class_id`) USING BTREE,
  UNIQUE INDEX `class_name`(`class_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '班级' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `msg_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '消息id',
  `msg_from_id` int(0) NOT NULL COMMENT '发送方',
  `msg_to_id` int(0) NOT NULL COMMENT '接收方',
  `msg_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '日期',
  `msg_detail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容',
  `msg_read` enum('未读','已读') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '未读' COMMENT '是否已读',
  PRIMARY KEY (`msg_id`) USING BTREE,
  INDEX `msg_from_id`(`msg_from_id`, `msg_to_id`) USING BTREE,
  INDEX `msg_from_id_2`(`msg_from_id`, `msg_to_id`) USING BTREE,
  INDEX `msg_to_id`(`msg_to_id`) USING BTREE,
  INDEX `msg_from_id_3`(`msg_from_id`, `msg_to_id`) USING BTREE,
  CONSTRAINT `message_ibfk_1` FOREIGN KEY (`msg_from_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `message_ibfk_2` FOREIGN KEY (`msg_to_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '消息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for pap_que
-- ----------------------------
DROP TABLE IF EXISTS `pap_que`;
CREATE TABLE `pap_que`  (
  `pq_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '记录id',
  `pq_paper_id` int(0) NOT NULL COMMENT '试卷id',
  `pq_que_id` int(0) NOT NULL COMMENT '问题id',
  `pq_score` int(0) NOT NULL COMMENT '分值',
  `pq_no` int(0) NOT NULL COMMENT '题号',
  PRIMARY KEY (`pq_id`) USING BTREE,
  UNIQUE INDEX `pq_paper_id_2`(`pq_paper_id`, `pq_no`, `pq_que_id`) USING BTREE,
  UNIQUE INDEX `pq_paper_id_3`(`pq_paper_id`, `pq_no`) USING BTREE,
  INDEX `pq_paper_id`(`pq_paper_id`) USING BTREE,
  INDEX `pq_que_id`(`pq_que_id`) USING BTREE,
  CONSTRAINT `pap_que_ibfk_1` FOREIGN KEY (`pq_paper_id`) REFERENCES `paper` (`paper_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `pap_que_ibfk_2` FOREIGN KEY (`pq_que_id`) REFERENCES `question` (`que_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '试卷和题目的关系' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for paper
-- ----------------------------
DROP TABLE IF EXISTS `paper`;
CREATE TABLE `paper`  (
  `paper_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '试卷id',
  `paper_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '试卷标题',
  `paper_author_id` int(0) NOT NULL COMMENT '作者id',
  `paper_checker_id` int(0) NULL DEFAULT NULL COMMENT '审查id',
  `paper_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创作日期',
  `paper_start` datetime(0) NOT NULL COMMENT '开始日期',
  `paper_end` datetime(0) NOT NULL COMMENT '结束日期',
  `paper_sub_id` int(0) NOT NULL COMMENT '科目',
  `paper_status` enum('待审核','未通过','已通过','已失效') CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '待审核' COMMENT '审查状态',
  PRIMARY KEY (`paper_id`) USING BTREE,
  INDEX `paper_author_id`(`paper_author_id`) USING BTREE,
  INDEX `paper_checker_id`(`paper_checker_id`) USING BTREE,
  INDEX `paper_sub_id`(`paper_sub_id`) USING BTREE,
  CONSTRAINT `paper_ibfk_1` FOREIGN KEY (`paper_author_id`) REFERENCES `teacher` (`tea_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `paper_ibfk_2` FOREIGN KEY (`paper_checker_id`) REFERENCES `teacher` (`tea_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `paper_ibfk_3` FOREIGN KEY (`paper_sub_id`) REFERENCES `subject` (`sub_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '试卷' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for question
-- ----------------------------
DROP TABLE IF EXISTS `question`;
CREATE TABLE `question`  (
  `que_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '问题id',
  `que_type` enum('单选题','多选题','判断题','填空题','主观题') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '问题类型',
  `que_sub_id` int(0) NOT NULL COMMENT '科目id',
  `que_diff` int(0) NOT NULL COMMENT '难度',
  `que_author_id` int(0) NOT NULL COMMENT '作者',
  `que_date` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创作时间',
  `que_detail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '问题内容',
  `que_answer` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '答案',
  `que_ana` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '解析',
  PRIMARY KEY (`que_id`) USING BTREE,
  INDEX `que_sub_id`(`que_sub_id`) USING BTREE,
  INDEX `que_author_id`(`que_author_id`) USING BTREE,
  CONSTRAINT `question_ibfk_1` FOREIGN KEY (`que_sub_id`) REFERENCES `subject` (`sub_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `question_ibfk_2` FOREIGN KEY (`que_author_id`) REFERENCES `teacher` (`tea_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '问题' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for question_file
-- ----------------------------
DROP TABLE IF EXISTS `question_file`;
CREATE TABLE `question_file`  (
  `qf_id` int(0) NOT NULL,
  `qf_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`qf_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for report
-- ----------------------------
DROP TABLE IF EXISTS `report`;
CREATE TABLE `report`  (
  `report_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '成绩单id',
  `report_stu_id` int(0) NOT NULL COMMENT '成绩单学生id',
  `report_paper_id` int(0) NOT NULL COMMENT '成绩单试卷id',
  `report_score` int(0) NOT NULL COMMENT '成绩单',
  `report_total` int(0) NOT NULL COMMENT '总分',
  `report_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '生成时间',
  PRIMARY KEY (`report_id`) USING BTREE,
  UNIQUE INDEX `report_stu_id_2`(`report_stu_id`, `report_paper_id`) USING BTREE,
  INDEX `report_stu_id`(`report_stu_id`) USING BTREE,
  INDEX `report_paper_id`(`report_paper_id`) USING BTREE,
  CONSTRAINT `report_ibfk_1` FOREIGN KEY (`report_stu_id`) REFERENCES `student` (`stu_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `report_ibfk_2` FOREIGN KEY (`report_paper_id`) REFERENCES `paper` (`paper_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '成绩单' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `stu_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '学生id',
  `stu_user_id` int(0) NOT NULL COMMENT '用户id',
  `stu_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `stu_gender` enum('男','女') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别',
  `stu_class_id` int(0) NULL DEFAULT NULL COMMENT '班级',
  `stu_birthday` datetime(0) NULL DEFAULT NULL COMMENT '生日',
  `stu_email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电子邮箱',
  PRIMARY KEY (`stu_id`) USING BTREE,
  INDEX `stu_class_id`(`stu_class_id`) USING BTREE,
  INDEX `stu_user_id`(`stu_user_id`) USING BTREE,
  CONSTRAINT `student_ibfk_1` FOREIGN KEY (`stu_class_id`) REFERENCES `class` (`class_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `student_ibfk_2` FOREIGN KEY (`stu_user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学生表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for subject
-- ----------------------------
DROP TABLE IF EXISTS `subject`;
CREATE TABLE `subject`  (
  `sub_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '科目id',
  `sub_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '科目名称',
  PRIMARY KEY (`sub_id`) USING BTREE,
  UNIQUE INDEX `sub_name`(`sub_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `tea_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '教师id',
  `tea_user_id` int(0) NOT NULL COMMENT '用户id',
  `tea_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `tea_gender` enum('男','女') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别',
  `tea_birthday` datetime(0) NULL DEFAULT NULL COMMENT '生日',
  `tea_sub_id` int(0) NULL DEFAULT NULL COMMENT '科目',
  `tea_email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电子邮箱',
  PRIMARY KEY (`tea_id`) USING BTREE,
  INDEX `tea_sub_id`(`tea_sub_id`) USING BTREE,
  INDEX `tea_user_id`(`tea_user_id`) USING BTREE,
  CONSTRAINT `teacher_ibfk_1` FOREIGN KEY (`tea_sub_id`) REFERENCES `subject` (`sub_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `teacher_ibfk_2` FOREIGN KEY (`tea_user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '教师' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `user_password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `user_role` int(0) NOT NULL COMMENT '角色',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `user_name`(`user_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户' ROW_FORMAT = Compact;

-- ----------------------------
-- Event structure for check expired
-- ----------------------------
DROP EVENT IF EXISTS `check expired`;
delimiter ;;
CREATE EVENT `check expired`
ON SCHEDULE
EVERY ''0 0:1'' DAY_MINUTE STARTS '2024-04-28 18:19:34'
COMMENT '定期处理过期试卷'
DO UPDATE paper SET paper_status = '已失效' WHERE paper_end < CURRENT_TIMESTAMP
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
